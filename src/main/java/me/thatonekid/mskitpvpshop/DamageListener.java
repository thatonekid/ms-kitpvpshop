package me.thatonekid.mskitpvpshop;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {

    public Main plugin = Main.getPlugin(Main.class);

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event)
    {
        Entity entity = event.getEntity();
        if (entity.getUniqueId().toString().equals(plugin.getConfig().getString("npc.id")))
        {
            event.setDamage(0);
            event.setCancelled(true);
        }
    }
}
