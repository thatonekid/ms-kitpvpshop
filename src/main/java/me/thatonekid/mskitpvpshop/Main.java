package me.thatonekid.mskitpvpshop;

import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;

public class Main extends JavaPlugin {

    public Main plugin;
    public File configFile;

    public Economy econ = null;
    public Permission perms = null;

    public Functions func = new Functions(this);

    @Override
    public void onEnable()
    {
        if (!getDataFolder().exists())
        {
            getDataFolder().mkdirs();
        }

        configFile = new File(getDataFolder(), "config.yml");
        if (!this.configFile.exists())
        {
            genConfig();
        }
        if (!setupEconomy()){
            ShopLog.info("MS-KitPvP Shop has been disabled due to no Vault dependency.");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        setupPermissions();
        ShopLog.info("MS-KitPvP Shop has been enabled, created by Sobeman18/ThatOneKid.");
        generateNPC();

        getServer().getPluginManager().registerEvents(new DamageListener(), this);
        getServer().getPluginManager().registerEvents(new ShopListener(), this);
        getCommand("tppos").setExecutor(new TPPosCommand());
    }

    @Override
    public void onDisable()
    {
        ShopLog.info("MS-KitPvP Shop has been disabled, created by Sobeman18/ThatOneKid.");
        saveConfig();
    }

    public void genConfig()
    {
        saveConfig();
    }

    public void generateNPC()
    {
        for(Entity ent : getServer().getWorlds().get(0).getEntities())
        {
            ent.remove();
        }
        World world = Bukkit.getServer().getWorlds().get(0);
        double x = 104.359;
        double y = 74;
        double z = 262.460;
        Location npcSpawn = new Location(world, x, y, z);
        Villager villager = (Villager) world.spawnEntity(npcSpawn, EntityType.VILLAGER);
        villager.setCustomName(ChatColor.AQUA + "" + ChatColor.BOLD + "Shop Villager");
        villager.setCustomNameVisible(true);
        villager.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 10000000, 10000000));
        getConfig().createSection("npc");
        getConfig().set("npc.id", villager.getUniqueId().toString());
        saveConfig();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    //Please don't remove any credits this plugin belongs to Minestar AND Sobeman18/ThatOneKid.

}
