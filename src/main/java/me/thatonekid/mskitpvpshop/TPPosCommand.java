package me.thatonekid.mskitpvpshop;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TPPosCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.GREEN + "You can only use this command IN GAME.");
            return true;
        }
        if (args.length == 0)
        {
            return false;
        }
        double x;
        double y;
        double z;
        if (args.length > 4) {
            return false;
        }
        try{
            x = Math.max(0, Math.min(1000000, Double.parseDouble(args[0])));
            y = Math.max(0, Math.min(1000000, Double.parseDouble(args[1])));
            z = Math.max(0, Math.min(1000000, Double.parseDouble(args[2])));
        } catch (Exception e){
            sender.sendMessage(ChatColor.RED + "X Y Z must be a number!");
            return true;
        }
        Player player = (Player) sender;
        player.teleport(new Location(player.getWorld(), x, y, z));

        return true;
    }
}
