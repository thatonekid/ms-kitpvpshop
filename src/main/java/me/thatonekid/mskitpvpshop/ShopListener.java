package me.thatonekid.mskitpvpshop;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ShopListener implements Listener {

    public final static String GUIName = ChatColor.AQUA + "" + ChatColor.BOLD + "Shop";

    public static Inventory myInventory = (Inventory) Bukkit.createInventory(null, 9, GUIName);

    public Main plugin = Main.getPlugin(Main.class);

    @EventHandler
    public void onNPCClick(PlayerInteractEntityEvent event)
    {
        Player player = event.getPlayer();
        if (event.getRightClicked().getType().equals(EntityType.VILLAGER))
        {
            Villager v = (Villager) event.getRightClicked();
            if (v.getUniqueId().toString().equals(plugin.getConfig().getString(("npc.id"))))
            {
                event.setCancelled(true);
                player.openInventory(myInventory);
                Enderman();
                PVP();
                Bunny();
                Tank();
                OK();
            }
        }
    }


    @EventHandler
    public void onInventoryClick(InventoryClickEvent event)
    {
        ItemStack is = event.getCurrentItem();
        Player p = (Player) event.getWhoClicked();
        Inventory i = event.getInventory();
        double bal = plugin.econ.getBalance(p);
        double price = 100;
        if (i.getName().equals(GUIName))
        {
            event.setCancelled(true);
            if (!plugin.func.canAfford(bal, price))
            {
             p.closeInventory();
             p.sendMessage(ChatColor.RED + "You must have $100 to afford a kit!");
                return;
            }

            if (is.getItemMeta().getDisplayName().equals(ChatColor.RED + "" + ChatColor.BOLD + "Enderman"))
            {
              p.closeInventory();
              if (p.hasPermission("kingkits.permission.kit.Enderman"))
              {
                  p.sendMessage(ChatColor.RED + "You already have access to this kit!");
                  return;
              }
              plugin.perms.playerAdd(p, "kingkits.command.kit.Enderman");
              plugin.econ.withdrawPlayer(p, 100);
              p.sendMessage(ChatColor.RED + "You now have access to the Enderman kit.");
              return;
            }

            if (is.getItemMeta().getDisplayName().equals(ChatColor.GREEN + "" + ChatColor.BOLD + "PVP"))
            {
                p.closeInventory();
                if (p.hasPermission("kingkits.permission.kit.PVP"))
                {
                    p.sendMessage(ChatColor.RED + "You already have access to this kit!");
                    return;
                }
                plugin.perms.playerAdd(p, "kingkits.command.kit.PVP");
                plugin.econ.withdrawPlayer(p, 100);
                p.sendMessage(ChatColor.RED + "You now have access to the PVP kit.");
                return;
            }

            if (is.getItemMeta().getDisplayName().equals(ChatColor.AQUA + "" + ChatColor.BOLD + "Bunny"))
            {
                p.closeInventory();
                if (p.hasPermission("kingkits.permission.kit.Bunny"))
                {
                    p.sendMessage(ChatColor.RED + "You already have access to this kit!");
                    return;
                }
                plugin.perms.playerAdd(p, "kingkits.command.kit.Bunny");
                plugin.econ.withdrawPlayer(p, 100);
                p.sendMessage(ChatColor.RED + "You now have access to the Bunny kit.");
                return;
            }

            if (is.getItemMeta().getDisplayName().equals(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Tank"))
            {
                p.closeInventory();
                if (p.hasPermission("kingkits.permission.kit.Tank"))
                {
                    p.sendMessage(ChatColor.RED + "You already have access to this kit!");
                    return;
                }
                plugin.perms.playerAdd(p, "kingkits.command.kit.Tank");
                plugin.econ.withdrawPlayer(p, 100);
                p.sendMessage(ChatColor.RED + "You now have access to the Tank kit.");
                return;
            }

            if (is.getItemMeta().getDisplayName().equals(ChatColor.DARK_RED + "" + ChatColor.BOLD + "OK"))
            {
                p.closeInventory();
                if (p.hasPermission("kingkits.permission.kit.OK"))
                {
                    p.sendMessage(ChatColor.RED + "You already have access to this kit!");
                    return;
                }
                plugin.perms.playerAdd(p, "kingkits.command.kit.OK");
                plugin.econ.withdrawPlayer(p, 100);
                p.sendMessage(ChatColor.RED + "You now have access to the OK kit.");
                return;
            }
        }

    }




    public void Enderman()
    {
        ItemStack enderman = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta enderMeta = enderman.getItemMeta();
        enderMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Enderman");
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.BLUE + "Price: 100");
        enderMeta.setLore(lores);
        enderman.setItemMeta(enderMeta);
        myInventory.setItem(0, enderman);

    }
    public void PVP()
    {
        ItemStack pvp = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta pvpMeta = pvp.getItemMeta();
        pvpMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "PVP");
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.BLUE + "Price: 100");
        pvpMeta.setLore(lores);
        pvp.setItemMeta(pvpMeta);
        myInventory.setItem(1, pvp);
    }
    public void Bunny()
    {
        ItemStack bunny = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta bunnyMeta = bunny.getItemMeta();
        bunnyMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Bunny");
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.BLUE + "Price: 100");
        bunnyMeta.setLore(lores);
        bunny.setItemMeta(bunnyMeta);
        myInventory.setItem(2, bunny);
    }
    public void Tank()
    {
        ItemStack tank = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta tankMeta = tank.getItemMeta();
        tankMeta.setDisplayName(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Tank");
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.BLUE + "Price: 100");
        tankMeta.setLore(lores);
        tank.setItemMeta(tankMeta);
        myInventory.setItem(3, tank);

    }
    public void OK()
    {
        ItemStack ok = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta okMeta = ok.getItemMeta();
        okMeta.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "OK");
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.BLUE + "Price: 100");
        okMeta.setLore(lores);
        ok.setItemMeta(okMeta);
        myInventory.setItem(4, ok);

    }
}
